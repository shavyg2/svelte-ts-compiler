#!/usr/bin/env node

import program from "commander";
import * as yup from "yup";
import { CompilerFactory } from "../src/lib/CompileSvelte/CompilerFactory";

const version = require("../package.json").version

const parameters = program
.version(version)
.option("-s, --src <src>","Source Directory")
.option("-d, --dist <dist>","Destination Directory")
.parse(process.argv)


const config = yup.object().shape({
    src:yup.string().default("page"),
    dist:yup.string().default("src")
}).validateSync(parameters)

const compiler = new CompilerFactory().createCompiler(config)


compiler.watch().catch(error=>{
    console.log("Error running compiler")
    console.log(error.message);
    console.log(error.stack);
})


