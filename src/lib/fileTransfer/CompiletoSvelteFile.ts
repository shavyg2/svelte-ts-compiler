import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { TransformToAbsolutePath } from '../fileSystem/TransformToAbsolutePath';
import { ConvertFileToExtension } from '../fileMatch/Matches/ConvertFileToExtension';
import { GetContent } from '../fileSystem/GetContent';
export async function CompiletoSvelteFile(config: SvelteCompilerConfig, markup: string, script: string) {
    const outputSvelteFile = ConvertFileToExtension(TransformToAbsolutePath(markup), ".svelte").replace(TransformToAbsolutePath(config.src), TransformToAbsolutePath(config.dist));
    console.log(`compiling:${outputSvelteFile}`);
    const html = await GetContent(markup);
    const javascript = await GetContent(script);
    const output = `<script>
${javascript.trim()}
</script>

${html.trim()}`;
    return output;
}
