import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { TransformToAbsolutePath } from '../fileSystem/TransformToAbsolutePath';
import path from "path";
import { ConvertFileToExtension } from '../fileMatch/Matches/ConvertFileToExtension';
import { isTypeScript } from '../fileType/isScript';
export function ConvertFromSrcToDist(config: SvelteCompilerConfig, file: string) {
    
    const src = TransformToAbsolutePath(config.src);
    const dist = TransformToAbsolutePath(config.dist);
    const originalFile = TransformToAbsolutePath(file);
    const destinationPath = path.join(dist,originalFile.replace(src,""));

    let filename = isTypeScript(originalFile)? ConvertFileToExtension(destinationPath,".js"):destinationPath

    return filename;
}
