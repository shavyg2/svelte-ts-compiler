import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { ConvertFileToExtension } from '../fileMatch/Matches/ConvertFileToExtension';
import { ConvertFromSrcToDist } from './ConvertFromSrcToDist';
export function ConvertToSveltePath(config: SvelteCompilerConfig, file: string) {
    const dist = ConvertFromSrcToDist(config,file)
    
    const outputSvelteFile = ConvertFileToExtension(dist,".svelte");
    return outputSvelteFile;
}

