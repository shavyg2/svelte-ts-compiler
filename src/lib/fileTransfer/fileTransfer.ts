import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { CompareFile } from '../fileSystem/File';
import { FileExist } from '../fileSystem/FileExist';
import { File } from '../fileSystem/File';
import { config } from 'process';
import { CompiletoSvelteFile } from './CompiletoSvelteFile';
import { SetContent } from '../fileSystem/SetContent';
import { ConvertToSveltePath } from './ConvertToSveltePath';
import { isJavaScript, isScript,isTypeScript } from '../fileType/isScript';
import { ConvertFromSrcToDist } from './ConvertFromSrcToDist';
import { GetContent } from '../fileSystem/GetContent';
import { FileRequired } from '../fileSystem/fileRequired';
import { isMarkup } from '../fileType/isMarkup';
import { ConvertFileToExtension } from '../fileMatch/Matches/ConvertFileToExtension';

export interface IFileTransfer {
    transfer(): Promise<void>;
}

export class SvelteComponentTransfer implements IFileTransfer {
    constructor(
        private config: SvelteCompilerConfig,
        private markup: File,
        private script: CompareFile
    ) {}

    private async validateFiles() {
        if (!(await FileExist(this.markup))) {
            throw new Error(`${this.markup} does not exist`);
        }

        if (!(await FileExist(this.script))) {
            throw new Error(`${this.script} does not exist`);
        }
    }

    async transfer() {
        await this.validateFiles();
        const svelteFile = ConvertToSveltePath(this.config, this.markup);
        const svelteContent = await CompiletoSvelteFile(
            this.config,
            this.markup,
            this.script
        );
        await SetContent(svelteContent, svelteFile);
        console.log(`compiled:${svelteFile}`);
    }
}

export class ScriptComponentTransfer implements IFileTransfer {
    private file: string;
    constructor(private config: SvelteCompilerConfig, file: CompareFile) {
        if (!isScript(file)) {
            throw new Error(`${file} is not a script file`);
        }

        this.file = file
    }

    async transfer() {
        await this.validateFile();
        
        const filePath = ConvertFromSrcToDist(this.config, this.file);
        const content = await GetContent(this.file);
        if(content.trim()){
            await SetContent(content, filePath);
        }
        console.log(`compiled:${filePath}`);
    }

    private async validateFile() {
        if (!(await FileExist(this.file))) {
            throw new Error(`${this.file} does not exist`);
        }
    }
}
export class MarkupComponentTransfer implements IFileTransfer {
    private file: string;
    constructor(private config: SvelteCompilerConfig, file: File) {
        if (!isMarkup(file)) {
            throw new Error(`${file} is not a script file`);
        }

        this.file = file;
    }

    async transfer() {
        await this.validateFile();
        const filePath = ConvertFromSrcToDist(this.config, this.file);
        const content = await GetContent(this.file);
        const svelteOutput = ConvertFileToExtension(filePath, '.svelte');
        await SetContent(content, svelteOutput);
        console.log(`Compiled:${svelteOutput}`);
    }

    private async validateFile() {
        if (!(await FileExist(this.file))) {
            throw new Error(`${this.file} does not exist`);
        }
    }
}

export class FileTransferFactory {
    async getTransfer(config: SvelteCompilerConfig, path: string) {
        const [markup, script, typescript] = FileRequired(path);
        const markupExist = FileExist(markup);
        const JSscriptExist = FileExist(script);
        const typescriptExist = FileExist(typescript);

        const scriptExist = typescriptExist.then(result => {
            if (!result) {
                return JSscriptExist.then(result => {
                    return result ? script : "";
                });
            } else {
                return typescript;
            }
        });

        if ((await markupExist) && (await scriptExist)) {
            return new SvelteComponentTransfer(
                config,
                markup,
                await scriptExist
            );
        } else if (await scriptExist) {
            return new ScriptComponentTransfer(config, await scriptExist);
        } else if (await markupExist) {
            return new MarkupComponentTransfer(config, markup);
        } else {
            throw new Error(
                `Files are incorrect and can't be transfered\n${markup}\n${script}`
            );
        }
    }
}
