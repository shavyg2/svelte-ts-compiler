import { FileWatchingResult } from "./FileWatchingResult";
export type FilterFunction = (file: FileWatchingResult) => boolean;
