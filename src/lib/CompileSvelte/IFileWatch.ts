export interface IFileWatch {
    watch(): Promise<void>;
    close(): void;
}
