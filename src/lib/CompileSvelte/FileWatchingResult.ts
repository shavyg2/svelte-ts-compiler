import { File, CompareFile } from "../fileSystem/File";
import fs from "fs";
export type FileWatchingResult = {
    path: File | CompareFile;
    stats: fs.Stats;
    isNew?: boolean;
};
