import { isMarkup } from '../fileType/isMarkup';
import { isJavaScript } from '../fileType/isScript';
export function isSvelteBaseFile(file: string) {
    return isMarkup(file) || isJavaScript(file);
}
