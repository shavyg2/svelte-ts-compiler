import { FileWatchingResult } from "./FileWatchingResult";
export type OnFile = (file: FileWatchingResult) => Promise<void>;
