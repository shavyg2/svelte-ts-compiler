import { FileWatch } from './fileWatch';
import { isSvelteBaseFile } from './isSvelteBaseFile';
import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { CompileSvelte } from './CompileSvelte';
export class CompilerFactory {
    createCompiler(config: SvelteCompilerConfig) {
        return new FileWatch(config.src, CompileSvelte(config), ({ path }) => isSvelteBaseFile(path));
    }
}