import { OnFile } from './OnFile';
import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { FileTransferFactory } from '../fileTransfer/fileTransfer';
import { isProjectFile } from '../fileType/isProjectFile';
import { TransformToAbsolutePath } from '../fileSystem/TransformToAbsolutePath';



export function CompileSvelte(config:SvelteCompilerConfig){

    const reaction:OnFile =  async({path,stats,isNew})=>{


        try{
            if(isProjectFile(path)){
                path = TransformToAbsolutePath(path);
                const transferFactory = new FileTransferFactory();
                const transferApi = await transferFactory.getTransfer(config,TransformToAbsolutePath(path))
                await transferApi.transfer()
            }else{
                // console.log(`Ignored:${path}`)
            }
        }catch(e){
            console.log(e.stack);
            console.log(`${path} is invalid`)
        }
    }

    return reaction;
}



