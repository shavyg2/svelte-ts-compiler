import { IFileWatch } from "./IFileWatch";
import { OnFile } from "./OnFile";
import { FilterFunction } from "./FilterFunction";
import * as chokidar from "chokidar";
import fs from "fs";
import { TransformToAbsolutePath } from '../fileSystem/TransformToAbsolutePath';

export class FileWatch implements IFileWatch {
  private watcher!: chokidar.FSWatcher;
  constructor(
    private directory: string,
    private onFile: OnFile,
    private filter: FilterFunction
  ) {}
  async watch() {

    console.log(TransformToAbsolutePath(this.directory))

    this.watcher = this.watcher || chokidar.watch(this.directory,{
      alwaysStat:true
    })
    const watcher = this.watcher;


    watcher.on("add",(path,stats:fs.Stats)=>{
      this.onFile({path,stats,isNew:true})
    })
    
    watcher.on("change",(path,stats:fs.Stats)=>{
      this.onFile({path,stats})
    })
  }

  close() {
    if (this.watcher) {
      this.watcher.close();
      this.watcher = null as any;
    }
  }
}
