import { File } from "./File";
import path from "path";
export function TransformToAbsolutePath(file: File) {
    return path.isAbsolute(file) ? file : path.normalize(path.join(process.cwd(), file));
}
