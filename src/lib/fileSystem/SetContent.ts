import fs from "fs";
import util from "util";
import path from "path";
import mkdirp from "mkdirp";
import * as R from "rxjs"
import { mergeMap, retry,take } from "rxjs/operators";


import { FileExist } from "./FileExist";
export async function SetContent(data: string, file: string) {
    let folder = path.dirname(file)

    if(! await FileExist(folder)){
        await R.of(folder)
        .pipe(
            mergeMap((folder:string)=>{
                return util.promisify(mkdirp)(folder)
            }),
            retry(20)
        ).toPromise();

    }
    
    
    await R.of({
        data,file
    }).pipe(
        take(1),
        mergeMap(async ({data,file})=>{
            await util.promisify(fs.writeFile)(file, data);
        }),
        retry(20)
    ).toPromise()
}
