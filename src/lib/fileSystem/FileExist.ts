import { File, CompareFile } from "./File";
import fs from "fs";
import util from "util";
export function FileExist(file: File | CompareFile) {
    return util.promisify(fs.stat)(file)
        .then(() => {
            return true;
        })
        .catch(error => {
            return false;
        });
}
