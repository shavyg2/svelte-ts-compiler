import { isMarkup } from '../fileType/isMarkup';
import { ConvertFileToExtension } from '../fileMatch/Matches/ConvertFileToExtension';
import { isJavaScript, isTypeScript } from '../fileType/isScript';
import { TransformToAbsolutePath } from './TransformToAbsolutePath';

export function FileRequired(file: string) {
    file = TransformToAbsolutePath(file);
    if (isMarkup(file)) {
        return [
            file, 
            ConvertFileToExtension(file, '.js'),
            ConvertFileToExtension(file, '.ts'),
        ];
    } else if (isJavaScript(file)) {
        return [
            ConvertFileToExtension(file, '.html'),
            file,
            ConvertFileToExtension(file, '.ts')
        ];
    } else if (isTypeScript(file)) {
        return [
            ConvertFileToExtension(file, '.html'),
            ConvertFileToExtension(file, '.js'),
            file
        ];
    } else {
        throw new Error(`${file}\nIs not a valid file .svelte base file`);
    }
}
