
import { SvelteCompilerConfig } from '../config/SvelteCompilerConfig';
import { FileWatch } from '../CompileSvelte/fileWatch';
import { CompileSvelte } from '../CompileSvelte/CompileSvelte';
import { isSvelteBaseFile } from '../CompileSvelte/isSvelteBaseFile';
export class CompilerFactory {
    createCompiler(config: SvelteCompilerConfig) {
        return new FileWatch(config.src, CompileSvelte(config), ({ path }) => isSvelteBaseFile(path));
    }
}
