import path from "path";
export function ConvertFileToExtension(file: string, extention: string) {
    const filename = path.basename(file).split(".").slice(0, -1).join(".");
    const dir = path.dirname(file);
    return path.join(dir, `${filename}${extention}`);
}
