import { File, CompareFile } from "../../fileSystem/File";
import { TransformToAbsolutePath } from '../../fileSystem/TransformToAbsolutePath';
import { ConvertFileToExtension } from "./ConvertFileToExtension";
export async function TypescriptMatchesMarkup(file: File, file2: CompareFile) {
    const markupFile = ConvertFileToExtension(file,".html")
    return TransformToAbsolutePath(markupFile) === TransformToAbsolutePath(file2);
}




