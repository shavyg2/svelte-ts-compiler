import { File, CompareFile } from "../../fileSystem/File";
import { ConvertFileToExtension } from './ConvertFileToExtension';
export async function MarkupMatchesTypescript(file: File, file2: CompareFile) {
    const javascriptFile = ConvertFileToExtension(file,".js")
    return javascriptFile === file2;
}
