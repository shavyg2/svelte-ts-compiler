import { FileMatchFactory } from '../FileMatchFactory';



describe("FileMatch",()=>{


    test("Script File Change",async ()=>{
        const factory = new FileMatchFactory()
        const match_api =  factory.ScriptMatchesMarkup()
        const result = await match_api.matches("file.js","file.html")
        expect(result).toBe(true)
    })


    test("Markup File Change",async ()=>{
        const factory = new FileMatchFactory()
        const match_api =  factory.MarkupMatcher()
        const result = await match_api.matches("file.html","file.js")
        expect(result).toBe(true)
    })
})