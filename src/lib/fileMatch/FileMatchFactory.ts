import { TypescriptMatchesMarkup } from "./Matches/TypescriptMatchesMarkup";
import { MarkupMatchesTypescript } from "./Matches/MarkupMatchesTypescript";
import { FileMatcher } from "./filematch";

import { TransformToAbsolutePath } from "../fileSystem/TransformToAbsolutePath";
import { File, CompareFile } from "../fileSystem/File";
import fs from "fs";
import { FileExist } from '../fileSystem/FileExist';

export class FileSystemFileMatcher{

}



export class FileMatchFactory {
    MarkupMatcher() {
        return new FileMatcher(MarkupMatchesTypescript,TransformToAbsolutePath);
    }

    ScriptMatchesMarkup(){
        return new FileMatcher(TypescriptMatchesMarkup,TransformToAbsolutePath)
    }

    FileSystemMatchup(){
        return new FileMatcher(SvelteFileExist,TransformToAbsolutePath)
    }
}

export async function SvelteFileExist(file:File,file2:CompareFile){
    const correctFiles = (await Promise.all([FileExist(file),FileExist(file2)]))
    .reduce((a,b)=>{
        return a && b
    })
    return correctFiles;
}

