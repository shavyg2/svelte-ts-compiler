import { IFileMatch } from "./IFileMatch";
import { FileMatchLogic } from "./FileMatchLogic";
import { File } from "../fileSystem/File";

export type FilePrep = any

export class FileMatcher {
    constructor(private isMatch:FileMatchLogic,private format:FilePrep){

    }
    matches(file:File,compare:File){
        return this.isMatch(
            this.format(file),
            this.format(compare)
        );
    }

}


