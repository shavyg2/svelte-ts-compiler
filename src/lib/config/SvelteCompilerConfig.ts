export interface SvelteCompilerConfig {
    src: string;
    dist: string;
}
