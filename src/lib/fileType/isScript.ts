

export function isScript(file:string){
    return isJavaScript(file) || isTypeScript(file)
}

export function isJavaScript(file: string) {
    return /\.js$/.test(file);
}


export function isTypeScript(file:string){
    return /\.ts$/.test(file);
}
