import { isJavaScript, isTypeScript } from "./isScript";
import { isMarkup } from "./isMarkup";

export function isProjectFile(file:string){
    return isJavaScript(file) || isMarkup(file) || isTypeScript(file)
}